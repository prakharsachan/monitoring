main()
{

ansible-playbook -i hosts playbook.yml 

count=$(awk 'END{print NR}' hosts)
health="Healthy"
reason[0]=""

cpu_load_threshold=$(awk -F ':' '{ if ( $1 == "cpu_load" ) { print $2 } }' threshold.txt)
memory_usage_threshold=$(awk -F ':' '{ if ( $1 == "memory_usage" ) { print $2 } }' threshold.txt)
disk_usage_threshold=$(awk -F ':' '{ if ( $1 == "disk_usage" ) { print $2 } }' threshold.txt)

echo "Server,CPU Load,Memory Usage,Disk Usage,Health,Reason" > status.csv

for (( i=1; i<=$count; i++ ))
    do
    server_ip=$(awk -v i=$i 'NR==i {print $1}' hosts)

    cpu_load=$(awk '{ sum+=$9} END {print sum}' /tmp/monitoring/stat_top-$server_ip.txt)
    if (( $(echo "$cpu_load > $cpu_load_threshold" |bc -l) ))
    then
        health="UnHealthy"  
        reason[1]="CPU"
    else
        reason[1]=""
    fi

    memory_usage=$(awk '{ sum+=$10} END {print sum}' /tmp/monitoring/stat_top-$server_ip.txt)
    if (( $(echo "$memory_usage > $memory_usage_threshold" |bc -l) ))
    then
        health="UnHealthy"  
        reason[2]="Memory"
    else
        reason[2]=""
    fi

    disk_usage=$(awk '{print $5}' /tmp/monitoring/stat_df-$server_ip.txt | sed 's/%//')
    if (( $(echo "$disk_usage > $disk_usage_threshold" |bc -l) ))
    then
        health="UnHealthy"  
        reason[3]="Disk"
    else
        reason[3]=""
    fi
    
    echo "$server_ip,$cpu_load,$memory_usage,$disk_usage,$health,${reason[*]}" >> status.csv
    done

}
main